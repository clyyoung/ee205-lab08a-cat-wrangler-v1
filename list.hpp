///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// Header file for List class
///
/// @file list.hpp
/// @version 1.0
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   02 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

class DoubleLinkedList {
protected:
   Node* head = nullptr;
   Node* tail = nullptr;

public:
   const bool  empty() const;
   void        push_front( Node* newNode );
   Node*       pop_front();
   Node*       get_first() const;
   Node*       get_next( const Node* currentNode ) const;
   size_t      size() const;

   void        push_back( Node* newNode );
   Node*       pop_back();
   Node*       get_last() const;
   Node*       get_prev( const Node* currentNode ) const;
   void        insert_after( Node* currentNode, Node* newNode );
   void        insert_before( Node* currentNode, Node* newNode );
};
