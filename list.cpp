///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// File for List class
///
/// @file list.cpp
/// @version 1.0
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   02 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include <cassert>
#include "list.hpp"

using namespace std;

const bool DoubleLinkedList::empty() const{
   return head == nullptr;
}

void DoubleLinkedList::push_front(Node* newNode){
   if(newNode == nullptr)
      return;

   if(head != nullptr){          //list is not empty
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   } else {                      //list is empty, head and tail are at same node
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
}

Node* DoubleLinkedList::pop_front(){
   if(head == nullptr)
      return nullptr;
   Node* temp = head;
   head = temp->next;
   return temp;
}

Node* DoubleLinkedList::get_first() const{
   return head;
}

Node* DoubleLinkedList::get_next(const Node* currentNode) const{
   Node* node = head;
   while(node != currentNode){
      node = node->next;
   }
   return node->next;
}

size_t DoubleLinkedList::size() const{
   size_t i = 0;
   Node* node = head;
   while(node != nullptr){
      i++;
      node = node->next;
   }
   return i;
}

void DoubleLinkedList::push_back(Node* newNode){
  if(newNode == nullptr)
      return;

   if(head != nullptr){          //list is not empty
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
   } else {                      //list is empty, head and tail are at same node
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
}

Node* DoubleLinkedList::pop_back(){
   if(head == nullptr)  //return nullptr if list is empty
      return nullptr;
   Node* temp = tail;   //temp node hold tail data
   tail = temp->prev;   //new tail is node before temp
   return temp;         //return temp node with old tail's data

}

Node* DoubleLinkedList::get_last() const{
   return tail;
}

Node* DoubleLinkedList::get_prev(const Node* currentNode) const{
   Node* node = head;   //start at beginning of list
   while(node != currentNode){   //go through list until at currentNode
      node = node->next;
   }
   return node->prev;   //return node before currentNode

}

void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);    //list is empty, so just add node to front
      return;
   }

   if(currentNode != nullptr && head == nullptr)
      assert(false);       //currentNode cannot hold data since list is empty

   if(currentNode == nullptr && head != nullptr)
      assert(false);       //currentNode must hold data if list isn't empty   

   if(tail != currentNode){   //NOT inserting at end of list
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
   } else {                   //inserting at end of list
      push_back(newNode);
   }
}

void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);    //list is empty, so just add node to front
      return;
   }

   if(currentNode != nullptr && head == nullptr)
      assert(false);       //currentNode cannot hold data since list is empty

   if(currentNode == nullptr && head != nullptr)
      assert(false);       //currentNode must hold data if list isn't empty   

   if(head != currentNode){   //NOT inserting at beginning of list
      newNode->prev = currentNode->prev;
      currentNode->prev = newNode;
      newNode->next = currentNode;
      newNode->next->prev = newNode;
   } else {                   //inserting at beginning of list
      push_front(newNode);
   }
  
}
