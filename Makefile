###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author Christianne Young <clyyoung@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   02 Apr 2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

all: main

main.o:  main.cpp cat.hpp node.hpp list.hpp queue.hpp
	$(CXX) -c $(CXXFLAGS) $<

test.o:  test.cpp cat.hpp node.hpp list.hpp queue.hpp
	$(CXX) -c $(CXXFLAGS) $<

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

node.o: node.cpp node.hpp
	$(CXX) -c $(CXXFLAGS) $<

list.o: list.cpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

queue.o: queue.cpp queue.hpp
	$(CXX) -c $(CXXFLAGS) $<

main: main.cpp *.hpp main.o cat.o node.o list.o queue.o
	g++ -o main main.o cat.o node.o list.o queue.o

test: test.cpp *.hpp test.o cat.o node.o list.o queue.o
	g++ -o test test.o cat.o node.o list.o queue.o

clean:
	rm -f *.o main the
