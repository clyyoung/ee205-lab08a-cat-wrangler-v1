///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// Header file for Node class
///
/// @file node.hpp
/// @version 1.0
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   02 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

class Node{
   friend class DoubleLinkedList;
protected:
   Node* next = nullptr;
   Node* prev = nullptr;
};
