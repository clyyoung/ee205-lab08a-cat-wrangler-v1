///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.cpp
/// @version 1.0
///
/// Exports data about all queues
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   02 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>

#include "queue.hpp"

using namespace std;


